Flickr Attach
http://drupal.org/project/flickr_attach
=======================================


DESCRIPTION
------------
Flickr Attach adds a fieldset to the node form allowing users to attach flickr images or albums. Flickr Attach is configurable globally and by content type. 


REQUIREMENTS
------------
Drupal 6.x
Flickr module http://drupal.org/project/flickr


INSTALLING
----------
1. Copy the 'flickr_attach' folder to your sites/all/modules directory.
2. Go to Administer > Site building > Modules. Enable the module.
Read more about installing modules at http://drupal.org/node/70151


CONFIGURING AND USING
---------------------
1. Configure Flickr module at admin/settings/flickr. Even though it's optional for Flickr module, you must input a Default Flickr User ID for Flickr Attach to work properly. Note that the "Default Flickr User Id:" is different than your Flickr screen name. If Drupal returns the following error "Flickr error 2: Unknown user". Then go to the following page and review the user id info on the right. Under "Your user ID:" section. http://www.flickr.com/services/api/explore/?method=flickr.photos.getAllContexts

2. Go to Administer > User management > Permissions. Under section 'flickr_attach module' set appropriate permissions.

3. Flickr module attached images will not show up in the node display by default. You must enable the flickr_attach block, or create a View. To enable a block go to admin/build/block configure "flickr_attach: Flickr_attach" block. Enable other Flick blocks if needed.

4. Now you can attach a flickr image to your nodes. To do so go to node/add/page
Scroll down the page. Click on "Flickr Attachment" link. Select a photo or a set of photos (album).

Note: To disable Flickr Attach, use the Admin > Content Type > Edit Content Type form and uncheck the box "Yes, allow Flickr Image & Album Attachments" box in the Flickr Attach fieldset.

Note: Each Drupal user can have is own Flickr account at user/*/edit


REPORTING ISSUE. REQUESTING SUPPORT. REQUESTING NEW FEATURE
-----------------------------------------------------------
1. Go to the module issue queue at http://drupal.org/project/issues/flickr_attach?status=All&categories=All
2. Click on CREATE A NEW ISSUE link.
3. Fill the form.
4. To get a status report on your request go to http://drupal.org/project/issues/user


UPGRADING
---------
1. One of the most IMPORTANT things to do BEFORE you upgrade, is to backup your site's files and database. More info: http://drupal.org/node/22281
2. Disable actual module. To do so go to Administer > Site building > Modules. Disable the module.
3. Just overwrite (or replace) the older module folder with the newer version.
4. Enable the new module. To do so go to Administer > Site building > Modules. Enable the module.
5. Run the update script. To do so go to the following address: www.yourwebsite.com/update.php
Follow instructions on screen. You must be log in as an administrator (user #1) to do this step.

Read more about upgrading modules: http://drupal.org/node/250790
